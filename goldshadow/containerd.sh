#!/bin/sh
# =====  Version   =====
containerd_version=1.6.15
runc_version=1.1.4
cni_plugins_version=1.2.0

# =====  Prepare   =====
# -----  Package   -----
sudo apt update
sudo apt install -y curl wget tar lsb-release
# -----  Platform  -----
arch=$(dpkg --print-architecture)
os_codename=$(lsb_release -cs)

# =====  Install   =====
# ----- containerd -----
wget https://github.com/containerd/containerd/releases/download/v${containerd_version}/containerd-${containerd_version}-linux-${arch}.tar.gz
# wget https://github.com/containerd/containerd/releases/download/v${containerd_version}/containerd-${containerd_version}-linux-${arch}.tar.gz.sha256sum
# shasum -a 256 -c containerd-${containerd_version}-linux-${arch}.tar.gz.sha256sum
sudo tar zxCvf /usr/local containerd-${containerd_version}-linux-${arch}.tar.gz
# -----    runc    -----
wget https://github.com/opencontainers/runc/releases/download/v${runc_version}/runc.${arch}
sudo install -m 755 runc.${arch} /usr/local/sbin/runc
# ----- CNI plugin -----
wget https://github.com/containernetworking/plugins/releases/download/v${cni_plugins_version}/cni-plugins-linux-${arch}-v${cni_plugins_version}.tgz
sudo mkdir -p /opt/cni/bin
sudo tar zxCvf /opt/cni/bin cni-plugins-linux-${arch}-v${cni_plugins_version}.tgz

# ===== Set config =====
# ----- containerd -----
sudo mkdir -p /etc/containerd
containerd config default | sudo tee /etc/containerd/config.toml
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
# -----  systemd   -----
sudo curl -L https://raw.githubusercontent.com/containerd/containerd/main/containerd.service -o /etc/systemd/system/containerd.service
sudo systemctl daemon-reload
sudo systemctl enable --now containerd
systemctl status --no-pager containerd

# =====   Clean   =====
rm containerd-${containerd_version}-linux-${arch}.tar.gz runc.${arch} cni-plugins-linux-${arch}-v${cni_plugins_version}.tgz
