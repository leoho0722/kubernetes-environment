#!/bin/sh
# =====  Version   =====
# k8s_version=1.25.6-00
k8s_version=1.22.17-00

# =====  Prepare   =====
# -----  Package   -----
sudo apt update
sudo apt install -y curl ca-certificates apt-transport-https

# =====  Install   =====
# ----- Kubernetes -----
curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmor -o /usr/share/keyrings/kubernetes-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list > /dev/null
sudo apt update
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
# apt-cache show kubectl | grep Version | awk {'print $2'}
sudo apt install -y kubelet=${k8s_version} kubectl=${k8s_version} kubeadm=${k8s_version}
sudo apt-mark hold kubelet kubeadm kubectl

# ===== Set config =====
# ----- Kubernetes -----
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
sudo modprobe overlay
sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
sudo sysctl --system

# =====  Cluster   =====
if [ $# -gt 0 ]
then
	sudo hostnamectl set-hostname $1
	shift
	if [ $# = 0 ]
	then
		sudo kubeadm init --v=5 --pod-network-cidr=10.244.0.0/16
		mkdir -p $HOME/.kube
		sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
		sudo chown $(id -u):$(id -g) $HOME/.kube/config
		# kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml
		kubectl apply -f https://github.com/flannel-io/flannel/releases/latest/download/kube-flannel.yml
		kubectl cluster-info
		# kubectl taint node k8s-master node-role.kubernetes.io/master-
	else
		sudo kubeadm join $*
	fi
fi
