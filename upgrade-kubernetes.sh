#!/bin/sh

# ===== 參數定義 =====

KUBEADM_VERSION=1.28.0         # 定義要更新的 Kubernetes 版本號
KUBERNETES_NODE_NAME=leoho-k8s # 定義要重新連結的 Kubernetes 節點名稱
KUBERNETES_OUTPUT_FORMAT=yaml  # 定義輸出格式

# ===== 開始更新 Kubernetes =====

echo "===== 開始更新 Kubernetes ====="

echo "目前更新狀態：「更新 kubeadm 版本」"

# 更新 kubeadm 版本
sudo apt-mark unhold kubeadm
sudo apt-get update
sudo apt-get install -y kubeadm='${KUBEADM_VERSION}-*'
sudo apt-mark hold kubeadm

echo "目前更新狀態：「查看 kubeadm 版本，是否更新到指定版本」"

# 查看 kubeadm 版本，是否更新到指定版本
kubeadm version -o=${KUBERNETES_OUTPUT_FORMAT}

echo "目前更新狀態：「檢查當前 Kubernetes 叢集是否可以升級」"

# 檢查當前 Kubernetes 叢集是否可以升級
kubeadm upgrade plan

echo "目前更新狀態：「升級 Kubernetes 叢集」"

# 升級 Kubernetes 叢集
sudo kubeadm upgrade apply v${KUBEADM_VERSION}

echo "目前更新狀態：「更新 kubelet 和 kubectl 版本」"

# 更新 kubelet 和 kubectl 版本
sudo apt-mark unhold kubelet kubectl
sudo apt-get update
sudo apt-get install -y kubelet kubectl
sudo apt-mark hold kubelet kubectl

echo "目前更新狀態：「重新啟動 kubelet 服務」"

# 重新啟動 kubelet 服務
sudo systemctl daemon-reload
sudo systemctl restart kubelet

echo "目前更新狀態：「重新連結 Kubernetes 節點」"

# 重新連結 Kubernetes 節點
kubectl uncordon ${KUBERNETES_NODE_NAME}

echo "目前更新狀態：「查看 Kubernetes 節點狀態」"

# 查看 Kubernetes 節點狀態
kubectl get nodes -o=wide

# ===== Kubernetes 更新完成 =====

echo "===== Kubernetes 更新完成 ====="

kubectl cluster-info