# Kubernetes Environment

## Install

* Container Runtime
  * containerd
    * latest version: 1.7.9
    * <https://github.com/containerd/containerd/releases/latest/>
  * runc
    * latest version: 1.1.10
    * <https://github.com/opencontainers/runc/releases/latest/>
  * CNI Plugin
    * latest version: 1.3.0
    * <https://github.com/containernetworking/plugins/releases/latest/>
* Kubernetes
  * latest version: 1.28.4
  * <https://github.com/kubernetes/kubernetes/releases/latest/>
    * flannel
      * latest version: 0.23.0
      * <https://github.com/flannel-io/flannel/releases/latest/>

## Scripts Description

* [containerd-latestVersion.sh](./containerd-latestVersion.sh)
  * description: Install latest version of containerd
* [kubernetes-latestVersion.sh](./kubernetes-latestVersion.sh)
  * description: Install latest version of Kubernetes
* [checkVersion-kubeadm.sh](./checkVersion-kubeadm.sh)
  * description: Check latest version of kubeadm
* [upgrade-kubernetes.sh](./upgrade-kubernetes.sh)
  * description: Upgrade latest version of Kubernetes into current Kubernetes Cluster
